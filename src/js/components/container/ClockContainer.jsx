import React,{ Component } from "react";
import ReactDOM from "react-dom";

class ClockContainer extends Component {
    constructor() {
        super();

        this.state = {
            time: 0
        };

        this.tick = this.tick.bind(this);
    }

    tick() {
        this.setState({
            time: this.state.time + 1
        })
    }

    componentDidMount() {
        console.log('component did mount');
        this.timer = setInterval(this.tick, 100);
    }
    
    componentWillUnmount() {
        console.log('component will unmount');
        clearInterval(this.timer)
    }

    render() {
        const { time } = this.state;

        return (
            <div className="col-12 alert alert-success float-right" role="alert">
                <span className="float-left">Times is running out :</span><span className="float-right"> { time } seconds. </span>
            </div>
        );
    }
}

export default ClockContainer;

const wrapper = document.getElementById("clock-react");
wrapper ? ReactDOM.render(<ClockContainer />, wrapper):"<strong> Wrong </strong>";
