import React from "react";
import RDom from "react-dom";

const App = () => {
    return (
        <div>
            <p id='welcome'>React Here! with Webpack, and Babel.</p>
        </div>
    );
};

export default App;

RDom.render(<App />, document.getElementById("app"));