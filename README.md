# This is example React, WebPack and Babel

~~note - command for packages :~~
~~npm i @babel/core babel-loader @babel/preset-env --save-dev~~
~~npm i react react-dom --save-dev~~
~~npm i @babel/preset-react --save-dev~~
~~npm i html-webpack-plugin html-loader --save-dev~~
~~npm i mini-css-extract-plugin css-loader --save-dev~~
~~npm i webpack-dev-server --save-dev~~

For running this repo :
```
npm install
node run start
```

### Ref: 
- https://webpack.js.org/concepts/
- https://babeljs.io/docs/en/
- https://reactjs.org/docs/getting-started.html
- https://www.valentinog.com/blog/webpack-tutorial/